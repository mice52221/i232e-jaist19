#!/usr/bin/python
import math
pygx = [3/19.0, 3/19.0, 2/19.0, 3/19.0, 4/19.0, 4/19.0,\
4/18.0, 3/18.0, 4/18.0, 3/18.0, 3/18.0, 1/18.0,\
1/15.0, 4/15.0, 1/15.0, 4/15.0, 2/15.0, 3/15.0,\
4/15.0, 2/15.0, 4/15.0, 1/15.0, 1/15.0, 3/15.0]
px = [1/4.0, 1/4.0, 1/4.0, 1/4.0]
h = 0
for i in range(4):
  start = i * 6
  hxi = 0
  for j in range(6):
    hxi = hxi - pygx[start + j] * math.log(pygx[ start + j], 2)
  h = h + px[i] * hxi

print(h)
