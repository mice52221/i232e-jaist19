# Created by duongtd at 2020/01/04
#!/usr/bin/python

# please install requirements first (by running 'pip3 install -r requirements.txt')

import numpy as np
from math import log2

def main():
    P = np.array([[0.180, 0.274, 0.426, 0.120],\
       [0.171, 0.367, 0.274, 0.188],\
       [0.161, 0.339, 0.375, 0.125],\
       [0.079, 0.355, 0.384, 0.182]])
    Q = P - np.identity(4)
    Q[:, 0] = 1
    Q_inverse = np.linalg.inv(Q)
    z = np.matmul(np.array([1,0,0,0]), Q_inverse)
    print("The stationary distribution z = " + str(z))

    ### find entropy of X
    plogp = lambda p: (p*log2(p))
    H = 0
    for fi in z:
        H = H + plogp(fi)
    print("The entropy of X is: " + str(-H))

    ### find entropy rate H(X)
    HX = 0
    for fi in range(4):
        temp = 0
        for se in range(4):
            temp = temp + plogp(P[fi,se])
        HX = HX + z[fi] * temp
    print("The entropy rate H(X) is: " + str(-HX))

if __name__ == "__main__":
    main()