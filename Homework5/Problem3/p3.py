# Created by duongtd at 2019/12/27
#!/usr/bin/python

# please run with python3, and make sure run "pip3 install -r requirements.txt" first to install all dependencies

from math import log2
from math import ceil

# binary entropy function
from scipy.special import comb

def h(p):
    return -p * log2(p) - (1 - p) * log2(1 - p)

def main():
    px0 = 0.9
    px1 = 1 - px0
    n = 25
    e = 0.2

    # calculate true entropy
    HX = h(px0)
    # print(HX)

    count_i = []
    # consider the sequence with the number of one is from 0 to 25
    for i in range(26):
        # probability of this kind of sequence (can contain more than one sequence)
        prob_i = px0**(n-i)*px1**i
        # sample entropy
        H_sample = -log2(prob_i)/n
        temp = abs(H_sample - HX)
        # print(temp)
        if temp < e:
            count_i.append(i)
    print("Sequences with " + " or ".join([str(x) for x in count_i]) + " one will be in this typical set")

    # probability of being in this typical set and
    # size of this typical set
    prob = 0
    size = 0
    for i in count_i:
        temp = comb(n, i, exact=True)
        prob += temp * px1**i * px0**(n-i)
        size += temp
    print("The probability of being in this typical set is: " + str(prob))
    print("The size of this typical set is: " + str(size))

    print("\n---end section a, the below is for section b---\n")
    # if x belongs to the typical set
    lx1 = ceil(log2(size)) + 1
    print("If x belongs to the typical set, the length lx = " + str(lx1))

    # if x does not belong to the typical set
    lx2 = ceil(log2(2**n)) + 1
    print("If x does not belong to the typical set, the length lx = " + str(lx2))

    # code rate R
    R = (lx1*(prob) + lx2*(1-prob))/n
    print ("The code rate R = " + str(R))

if __name__ == "__main__":
    main()