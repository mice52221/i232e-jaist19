# Created by duongtd at 2019/12/19
#!/usr/bin/python

# please run with python3, and make sure run "pip3 install -r requirements.txt" first to install numpy dependency

import matplotlib.pyplot as plt

from math import log2

# binary entropy function
import numpy as np

def h(p):
    return -p * log2(p) - (1 - p) * log2(1 - p)

def main():
    x = np.arange(0, 1, 0.01)
    y = [-h(0.1)+h(p+0.1-0.2*p) for p in x]
    plt.plot(x, y)
    plt.xlabel("p")
    plt.ylabel("I(X;Z|Y)")
    plt.show()

if __name__ == "__main__":
    main()