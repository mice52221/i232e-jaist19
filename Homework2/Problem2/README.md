### PLEASE follow the below steps to run code

- run `pip3 install -r requirements.txt`
- run `python3 p2_e.py` for section e of problem 2
- replace p2_e with `p2_f` or `p2_g` if you want to check section e or g

_Please run with python3_ 