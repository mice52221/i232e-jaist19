# created by @duongtd on 19/12/19

#!/usr/bin/python

# please run with python3, and make sure run "pip3 install -r requirements.txt" first to install numpy dependency
# run "./p2_e.py 100" if you want to get result with n=100

from numpy.random import choice
from numpy import sum
import sys

# number of experiments
NO_EPERS = 1000

#epsilon
EPSILON = 0.1

def p2(n = -1):
    if n is p2.__defaults__[0]:
        print("# use default n = 50")
        n = 50
    pX = [0.25, 0.5, 0.25]
    x_value = [1, 2, 3]
    EX = 2
    count = 0
    for i in range(NO_EPERS):
        X = choice(x_value, size=n, p=pX)
        Xbar = sum(X, axis=0)/n
        if (abs(Xbar - EX) <= 0.1):
            count += 1

    print("Number of times with n=" + str(n) + " is: " + str(count))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        try:
            n = int(sys.argv[1])
        except:
            print("Incorrect param n=" + str(n) + " (not an integer), use default n = 50")
            n = 50
    else:
        print("use default n = 50")
        n = 50
    p2(n)